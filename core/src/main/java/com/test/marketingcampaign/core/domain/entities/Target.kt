package com.test.marketingcampaign.core.domain.entities

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Target(
    val slug: String,
    val name: String
) : Parcelable {
    class TargetDiffCallback : DiffUtil.ItemCallback<Target>() {
        override fun areItemsTheSame(
            oldItem: Target,
            newItem: Target
        ): Boolean = oldItem.slug == newItem.slug

        override fun areContentsTheSame(
            oldItem: Target,
            newItem: Target
        ): Boolean = oldItem == newItem
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Target

        if (slug != other.slug) return false

        return true
    }

    override fun hashCode(): Int {
        return slug.hashCode()
    }
}