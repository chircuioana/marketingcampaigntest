package com.test.marketingcampaign.core.domain.repositories

import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.domain.entities.Channel
import com.test.marketingcampaign.core.domain.entities.Target
import com.test.marketingcampaign.core.utils.Resource

interface MarketingCampaignRepository {
    suspend fun fetchTargets(): Resource<Set<Target>>
    suspend fun fetchChannelsForTarget(target: String): Resource<List<Channel>>
    suspend fun fetchCampaignsForChannel(channel: String): Resource<List<Campaign>>
}