package com.test.marketingcampaign.core.data.model

import com.squareup.moshi.Json
import com.test.marketingcampaign.core.domain.entities.Channel
import com.test.marketingcampaign.core.utils.DomainEntity

data class ChannelsResponse(
    @Json(name = "data")
    val channels: List<ChannelResponse>
) : DomainEntity<List<Channel>> {
    override fun toDomain(): List<Channel> {
        return channels.map { Channel(it.slug, it.displayName) }
    }
}

data class ChannelResponse(
    @Json(name = "slug")
    val slug: String,
    @Json(name = "displayName")
    val displayName: String
)