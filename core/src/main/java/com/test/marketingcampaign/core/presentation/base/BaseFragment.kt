package com.test.marketingcampaign.core.presentation.base

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)

        super.onAttach(context)
    }

    fun showLoading(showLoading: Boolean) {
        (activity as? BaseActivity)?.showLoading(showLoading)
    }

    fun setToolbarTitle(title: String) {
        (activity as? ToolbarOwner)?.setTitle(title)
    }
}