package com.test.marketingcampaign.core.domain.entities

import androidx.recyclerview.widget.DiffUtil

data class ChannelForm(
    val channelSlug: String,
    val channelName: String,
    val availableTargets: MutableSet<Target>
) {
    var campaign: Campaign? = null

    class ChannelDiffCallback : DiffUtil.ItemCallback<ChannelForm>() {
        override fun areItemsTheSame(
            oldItem: ChannelForm,
            newItem: ChannelForm
        ): Boolean = oldItem.channelSlug == newItem.channelSlug

        override fun areContentsTheSame(
            oldItem: ChannelForm,
            newItem: ChannelForm
        ): Boolean = oldItem == newItem
    }
}