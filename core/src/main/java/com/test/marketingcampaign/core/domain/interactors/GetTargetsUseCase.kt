package com.test.marketingcampaign.core.domain.interactors

import com.test.marketingcampaign.core.domain.entities.ErrorEntity
import com.test.marketingcampaign.core.domain.entities.Target
import com.test.marketingcampaign.core.domain.repositories.MarketingCampaignRepository
import com.test.marketingcampaign.core.utils.FlowUseCase
import com.test.marketingcampaign.core.utils.IoDispatcher
import com.test.marketingcampaign.core.utils.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetTargetsUseCase @Inject constructor(
    private val repository: MarketingCampaignRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<GetTargetsUseCase.Params, Set<Target>>(dispatcher) {

    class Params

    override fun execute(parameters: Params): Flow<Resource<Set<Target>>> {
        return flow {
            try {
                emit(Resource.Loading)
                emit(repository.fetchTargets())

            } catch (e: Exception) {
                emit(Resource.Error(ErrorEntity.Unknown))
            }
        }
    }
}