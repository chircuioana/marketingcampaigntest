package com.test.marketingcampaign.core.presentation.base

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.widget.Toolbar
import dagger.android.support.DaggerAppCompatActivity

interface ToolbarOwner {
    fun setTitle(title: String)
}

abstract class BaseActivity : DaggerAppCompatActivity(), ToolbarOwner {

    protected abstract fun layoutBinding(): View

    protected open fun toolbar(): Toolbar? = null

    protected open fun showBackButton(): Boolean = false

    open fun showLoading(isShowLoading: Boolean) = Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutBinding())
        toolbar()?.let {
            setSupportActionBar(it)
            it.title = ""
            it.setNavigationOnClickListener {
                this.onBackPressed()
            }
        }
    }

    override fun setTitle(title: String) {
        Handler().post {
            toolbar()?.title = title
        }
    }
}