package com.test.marketingcampaign.core.utils

interface DomainEntity<T> {
    fun toDomain(): T
}
