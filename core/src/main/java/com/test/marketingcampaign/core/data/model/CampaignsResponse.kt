package com.test.marketingcampaign.core.data.model

import com.squareup.moshi.Json
import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.utils.DomainEntity

data class CampaignsResponse(
    @Json(name = "data")
    val campaigns: List<CampaignResponse>
) : DomainEntity<List<Campaign>> {
    override fun toDomain(): List<Campaign> {
        return campaigns.map { Campaign(it.slug, it.priceEuro, it.description) }
    }
}

data class CampaignResponse(
    @Json(name = "slug")
    val slug: String,
    @Json(name = "priceEuro")
    val priceEuro: Double,
    @Json(name = "description")
    val description: String
)