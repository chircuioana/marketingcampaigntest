package com.test.marketingcampaign.core.domain.entities

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.DiffUtil
import com.test.marketingcampaign.core.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Campaign(
    val slug: String,
    val valueInEuro: Double,
    val description: String
) : Parcelable {

    val displayName: String
        get() = "${valueInEuro} EUR"

    class CampaignDiffCallback : DiffUtil.ItemCallback<Campaign>() {
        override fun areItemsTheSame(
            oldItem: Campaign,
            newItem: Campaign
        ): Boolean = oldItem.slug == newItem.slug

        override fun areContentsTheSame(
            oldItem: Campaign,
            newItem: Campaign
        ): Boolean = oldItem == newItem
    }

    @DrawableRes
    fun getBackground(): Int {
        return when (valueInEuro.toInt()) {
            in 0..100 -> R.drawable.gradient_blue_green
            in 101..125 -> R.drawable.gradient_purple_blue
            in 126..150 -> R.drawable.gradient_purple_red
            else -> R.drawable.gradient_red_yellow
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Campaign

        if (slug != other.slug) return false

        return true
    }

    override fun hashCode(): Int {
        return slug.hashCode()
    }
}