package com.test.marketingcampaign.core.domain.entities

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CampaignReview(
    val channelSlug: String,
    val channelName: String,
    val availableTargets: Set<Target>,
    val campaign: Campaign
) : Parcelable {

    class CampaignDiffCallback : DiffUtil.ItemCallback<CampaignReview>() {
        override fun areItemsTheSame(
            oldItem: CampaignReview,
            newItem: CampaignReview
        ): Boolean = oldItem.channelSlug == newItem.channelSlug

        override fun areContentsTheSame(
            oldItem: CampaignReview,
            newItem: CampaignReview
        ): Boolean = oldItem == newItem
    }
}