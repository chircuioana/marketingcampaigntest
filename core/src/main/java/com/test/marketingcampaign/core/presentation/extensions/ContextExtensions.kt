package com.test.marketingcampaign.core.presentation.extensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.widget.Toast
import androidx.annotation.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment

fun Int.dpToPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Int.pxToDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()

fun Fragment.toast(message: String) {
    requireActivity().toast(message)
}

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.drawable(@DrawableRes drawableRes: Int): Drawable =
    ContextCompat.getDrawable(this, drawableRes) ?: throw Exception("Drawable not found")

@ColorInt
fun Context.color(@ColorRes colorRes: Int): Int = ContextCompat.getColor(this, colorRes)

fun Context.font(@FontRes fontRes: Int): Typeface =
    ResourcesCompat.getFont(this, fontRes) ?: throw Exception("Font not found")

fun Context.getDimension(@DimenRes dimenRes: Int): Int = resources.getDimension(dimenRes).toInt()

fun Context.sendEmail(email: String, subject: String, body: String) {
    val intent = Intent(Intent.ACTION_SENDTO)
    intent.data = Uri.parse("mailto:")
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(
        Intent.EXTRA_TEXT, body
    )
    try {
        ContextCompat.startActivity(
            this,
            Intent.createChooser(intent, "Choose an Email client :"),
            null
        )
    } catch (ex: ActivityNotFoundException) {
        (this as? Activity)?.toast("No e-mail client found on this device")
    }
}