package com.test.marketingcampaign.core.utils

import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.test.marketingcampaign.core.domain.entities.ErrorEntity
import okhttp3.ResponseBody
import retrofit2.Response

fun <T> DomainEntity<T>.mapToDomain(): T {
    return toDomain()
}

fun <T, U> Response<T>.mapToDomain(): Resource<U> where T : DomainEntity<U> {
    if (isSuccessful && this.body() != null) {
        return Resource.Success(this.body()!!.mapToDomain())
    }
    return Resource.Error(this.errorBody()?.mapToErrorDomain() ?: ErrorEntity.Unknown)
}

fun ResponseBody.mapToErrorDomain(): ErrorEntity {
    val moshi = Moshi.Builder().build()
    val jsonAdapter: JsonAdapter<ErrorEntity.ApiError> =
        moshi.adapter(ErrorEntity.ApiError::class.java)
    return jsonAdapter.fromJson(string())!!
}