package com.test.marketingcampaign.core.domain.interactors

import com.test.marketingcampaign.core.domain.entities.ChannelForm
import com.test.marketingcampaign.core.domain.entities.ErrorEntity
import com.test.marketingcampaign.core.domain.entities.Target
import com.test.marketingcampaign.core.domain.repositories.MarketingCampaignRepository
import com.test.marketingcampaign.core.utils.FlowUseCase
import com.test.marketingcampaign.core.utils.IoDispatcher
import com.test.marketingcampaign.core.utils.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetChannelsForTargetsUseCase @Inject constructor(
    private val repository: MarketingCampaignRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<GetChannelsForTargetsUseCase.Params, List<ChannelForm>>(dispatcher) {

    class Params(val targets: Array<Target>)

    override fun execute(parameters: Params): Flow<Resource<List<ChannelForm>>> {
        return flow {
            try {
                emit(Resource.Loading)

                val forms = mutableListOf<ChannelForm>()
                parameters.targets.forEach { target ->
                    when (val resource = repository.fetchChannelsForTarget(target.slug)) {
                        is Resource.Success -> {
                            resource.data.forEach { channel ->
                                updateChannelList(forms, channel.slug, channel.name, target)
                            }
                        }
                        else -> {
                            emit(resource as Resource<List<ChannelForm>>)
                        }
                    }
                }
                emit(Resource.Success(forms))
            } catch (e: Exception) {
                emit(Resource.Error(ErrorEntity.Unknown))
            }
        }
    }

    /**
     * Updates the list with the channels and the targets associated with that channel
     */
    private fun updateChannelList(
        initialList: MutableList<ChannelForm>,
        channelSlug: String,
        channelName: String,
        target: Target,
    ) {
        initialList.find { it.channelSlug == channelSlug }
            ?.apply {
                availableTargets.add(target)
            }
            ?: run {
                initialList.add(
                    ChannelForm(
                        channelSlug,
                        channelName,
                        mutableSetOf(target)
                    )
                )
            }
    }
}