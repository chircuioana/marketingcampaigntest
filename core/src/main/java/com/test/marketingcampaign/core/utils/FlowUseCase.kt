package com.test.marketingcampaign.core.utils

import android.util.Log
import com.test.marketingcampaign.core.domain.entities.ErrorEntity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn

/**
 * Executes business logic in its execute method and keep posting updates to the result as
 * [Resource<R>].
 * Handling an exception (emit [Resource.Error] to the result) is the subclasses's responsibility.
 */
abstract class FlowUseCase<in P, R>(private val coroutineDispatcher: CoroutineDispatcher) {

    @ExperimentalCoroutinesApi
    operator fun invoke(parameters: P): Flow<Resource<R>> {
        return execute(parameters)
            .catch { e ->
                emit(Resource.Error(ErrorEntity.Unknown))
            }
            .flowOn(coroutineDispatcher)
    }

    abstract fun execute(parameters: P): Flow<Resource<R>>
}