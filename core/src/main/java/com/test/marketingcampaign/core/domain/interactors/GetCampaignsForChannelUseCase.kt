package com.test.marketingcampaign.core.domain.interactors

import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.domain.entities.ErrorEntity
import com.test.marketingcampaign.core.domain.repositories.MarketingCampaignRepository
import com.test.marketingcampaign.core.utils.FlowUseCase
import com.test.marketingcampaign.core.utils.IoDispatcher
import com.test.marketingcampaign.core.utils.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetCampaignsForChannelUseCase @Inject constructor(
    private val repository: MarketingCampaignRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<GetCampaignsForChannelUseCase.Params, List<Campaign>>(dispatcher) {

    data class Params(val channel: String)

    override fun execute(parameters: Params): Flow<Resource<List<Campaign>>> {
        return flow {
            try {
                emit(Resource.Loading)
                emit(repository.fetchCampaignsForChannel(parameters.channel))

            } catch (e: Exception) {
                emit(Resource.Error(ErrorEntity.Unknown))
            }
        }
    }
}