package com.test.marketingcampaign.core.domain.entities

data class Channel(
    val slug: String,
    val name: String
)