package com.test.marketingcampaign.core.data.sources.remote

import com.test.marketingcampaign.core.data.model.CampaignsResponse
import com.test.marketingcampaign.core.data.model.ChannelsResponse
import com.test.marketingcampaign.core.data.model.TargetsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MarketingCampaignApi {
    @GET("targets.json")
    suspend fun getTargets(): Response<TargetsResponse>

    @GET("channels/{target}.json")
    suspend fun getChannelsForTarget(@Path("target") target: String): Response<ChannelsResponse>

    @GET("campaigns/{channel}.json")
    suspend fun getCampaignsForChannel(@Path("channel") channel: String): Response<CampaignsResponse>
}