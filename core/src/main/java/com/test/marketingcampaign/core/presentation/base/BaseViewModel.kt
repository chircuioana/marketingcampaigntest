package com.test.marketingcampaign.core.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.marketingcampaign.core.domain.entities.ErrorEntity
import com.test.marketingcampaign.core.presentation.livedata.SingleLiveEvent

abstract class BaseViewModel : ViewModel() {
    val isLoading: LiveData<Boolean> get() = _isLoading
    protected val _isLoading = MutableLiveData<Boolean>()

    val error: LiveData<ErrorEntity> get() = _error
    protected val _error = SingleLiveEvent<ErrorEntity>()
}