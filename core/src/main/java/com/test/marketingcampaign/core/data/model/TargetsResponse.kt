package com.test.marketingcampaign.core.data.model

import com.squareup.moshi.Json
import com.test.marketingcampaign.core.utils.DomainEntity
import com.test.marketingcampaign.core.domain.entities.Target

data class TargetsResponse(
    @Json(name = "data")
    val targets: List<TargetResponse>
) : DomainEntity<Set<Target>> {
    override fun toDomain(): Set<Target> {
        return targets.map { Target(it.slug, it.displayName) }.toSet()
    }
}

data class TargetResponse(
    @Json(name = "slug")
    val slug: String,
    @Json(name = "displayName")
    val displayName: String
)