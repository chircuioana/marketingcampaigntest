package com.test.marketingcampaign.core.data.di

import com.test.marketingcampaign.core.data.repositories.MarketingCampaignRepositoryImpl
import com.test.marketingcampaign.core.data.sources.remote.MarketingCampaignApi
import com.test.marketingcampaign.core.domain.repositories.MarketingCampaignRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun providesMarketingCampaignRepository(
        @Named("marketingCampaignApiDataSource") marketingCampaignApi: MarketingCampaignApi
    ): MarketingCampaignRepository {
        return MarketingCampaignRepositoryImpl(marketingCampaignApi)
    }
}