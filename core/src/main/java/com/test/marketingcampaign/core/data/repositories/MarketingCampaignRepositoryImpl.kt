package com.test.marketingcampaign.core.data.repositories

import com.test.marketingcampaign.core.data.sources.remote.MarketingCampaignApi
import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.domain.entities.Channel
import com.test.marketingcampaign.core.domain.entities.Target
import com.test.marketingcampaign.core.domain.repositories.MarketingCampaignRepository
import com.test.marketingcampaign.core.utils.Resource
import com.test.marketingcampaign.core.utils.mapToDomain
import javax.inject.Inject

class MarketingCampaignRepositoryImpl @Inject constructor(private val marketingCampaignApi: MarketingCampaignApi) :
    MarketingCampaignRepository {

    override suspend fun fetchTargets(): Resource<Set<Target>> =
        marketingCampaignApi
            .getTargets()
            .mapToDomain()

    override suspend fun fetchChannelsForTarget(target: String): Resource<List<Channel>> =
        marketingCampaignApi
            .getChannelsForTarget(target)
            .mapToDomain()

    override suspend fun fetchCampaignsForChannel(channel: String): Resource<List<Campaign>> =
        marketingCampaignApi
            .getCampaignsForChannel(channel)
            .mapToDomain()

}