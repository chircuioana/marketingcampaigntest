package com.test.marketingcampaign.ui.targets

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.test.marketingcampaign.core.domain.entities.Target
import com.test.marketingcampaign.databinding.ItemTargetBinding

class TargetsAdapter(
    private val onToggleTarget: (Target) -> Unit
) : ListAdapter<Target, TargetsAdapter.TargetViewHolder>(Target.TargetDiffCallback()) {

    var selectedTargets: Set<Target> = setOf()
        set(value) {
            if (field == value) return
            field = value
//            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TargetViewHolder =
        TargetViewHolder(
            ItemTargetBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: TargetViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class TargetViewHolder(private val itemBinding: ItemTargetBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(target: Target) = itemBinding.run {
            chipTarget.text = target.name
            chipTarget.setOnClickListener {
                onToggleTarget(target)
            }

            val isChecked = selectedTargets.contains(target)
            if (chipTarget.isChecked == isChecked) return@run
            chipTarget.isChecked = isChecked
        }
    }
}