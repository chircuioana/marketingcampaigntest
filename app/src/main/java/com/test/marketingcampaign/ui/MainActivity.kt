package com.test.marketingcampaign.ui

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.presentation.base.BaseActivity
import com.test.marketingcampaign.core.presentation.extensions.goneUnless
import com.test.marketingcampaign.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupWindow()
        setupStatusBar()
    }

    override fun onStart() {
        super.onStart()

        val navController = Navigation.findNavController(this, R.id.navHost)
        val appBarConfiguration =
            AppBarConfiguration.Builder(
                setOf(
                    R.id.targetsFragment,
                )
            )
                .build()
        toolbar()?.setupWithNavController(navController, appBarConfiguration)
    }

    override fun layoutBinding(): View {
        binding = ActivityMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun toolbar(): Toolbar? = binding.toolbar

    override fun showBackButton(): Boolean = true

    override fun showLoading(isShowLoading: Boolean) {
        binding.loadingOverlay.root.goneUnless(isShowLoading)
    }

    private fun setupWindow() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            statusBarColor = Color.TRANSPARENT
        }
    }

    private fun setupStatusBar() {
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            val height = resources.getDimensionPixelSize(resourceId)
            binding.toolbar.setPadding(0, height, 0, 0)
        }
    }
}