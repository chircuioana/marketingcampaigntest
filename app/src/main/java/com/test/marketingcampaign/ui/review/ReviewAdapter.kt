package com.test.marketingcampaign.ui.review

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.test.marketingcampaign.core.domain.entities.CampaignReview
import com.test.marketingcampaign.core.presentation.extensions.drawable
import com.test.marketingcampaign.databinding.ItemChannelBinding

class ReviewAdapter() :
    ListAdapter<CampaignReview, ReviewAdapter.ReviewViewHolder>(CampaignReview.CampaignDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ReviewViewHolder =
        ReviewViewHolder(
            ItemChannelBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ReviewViewHolder(private val itemBinding: ItemChannelBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(form: CampaignReview) = itemBinding.run {
            tvChannelName.text = form.channelName
            cgChannelAvailableTargets.removeAllViews()
            form.availableTargets.forEach {
                cgChannelAvailableTargets.addView(
                    Chip(itemView.context).apply {
                        text = it.name
                        isEnabled = false
                    })
            }
            cvContainer.isClickable = false
            tvCampaignText.text = form.campaign.displayName
            tvCampaignText.setTextColor(Color.WHITE)
            clCampaignContainer.background =
                itemView.context.drawable(form.campaign.getBackground())
        }
    }
}