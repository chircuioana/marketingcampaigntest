package com.test.marketingcampaign.ui.channels

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.domain.entities.ChannelForm
import com.test.marketingcampaign.core.presentation.extensions.color
import com.test.marketingcampaign.core.presentation.extensions.drawable
import com.test.marketingcampaign.databinding.ItemChannelBinding

class ChannelsAdapter(
    private val onChannelSelected: (ChannelForm) -> Unit
) : ListAdapter<ChannelForm, ChannelsAdapter.ChannelViewHolder>(ChannelForm.ChannelDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChannelViewHolder =
        ChannelViewHolder(
            ItemChannelBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ChannelViewHolder(private val itemBinding: ItemChannelBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(form: ChannelForm) = itemBinding.run {
            tvChannelName.text = form.channelName
            cgChannelAvailableTargets.removeAllViews()
            form.availableTargets.forEach {
                cgChannelAvailableTargets.addView(
                    Chip(itemView.context).apply {
                        text = it.name
                        isEnabled = false
                    })
            }
            cvContainer.setOnClickListener {
                onChannelSelected(form)
            }
            form.campaign?.let {
                tvCampaignText.text = it.displayName
                tvCampaignText.setTextColor(Color.WHITE)
                clCampaignContainer.background = itemView.context.drawable(it.getBackground())
            } ?: run {
                tvCampaignText.text = itemView.context.getString(R.string.label_no_campaign)
                tvCampaignText.setTextColor(Color.BLACK)
                clCampaignContainer.background =
                    ColorDrawable(itemView.context.color(R.color.grey_CCCCCC))
            }
        }
    }
}