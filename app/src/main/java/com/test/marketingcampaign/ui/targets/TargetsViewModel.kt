package com.test.marketingcampaign.ui.targets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.test.marketingcampaign.core.domain.interactors.GetTargetsUseCase
import com.test.marketingcampaign.core.domain.entities.Target as Target
import com.test.marketingcampaign.core.presentation.base.BaseViewModel
import com.test.marketingcampaign.core.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

class TargetsViewModel @Inject constructor(
    private val getTargetsUseCase: GetTargetsUseCase
) : BaseViewModel() {

    val targets: LiveData<List<Target>>
        get() = Transformations.map(_targets) {
            it.toList()
        }
    private val _targets = MutableLiveData<Set<Target>>()

    val selectedTargets: LiveData<Set<Target>> get() = _selectedTargets
    private val _selectedTargets = MutableLiveData<Set<Target>>()

    val isDoneEnabled: LiveData<Boolean> get() = Transformations.distinctUntilChanged(_isDoneEnabled)
    private val _isDoneEnabled = MutableLiveData<Boolean>()

    init {
        _selectedTargets.value = setOf()
        _isDoneEnabled.value = false
    }

    fun fetchTargets() = viewModelScope.launch {
        getTargetsUseCase.execute(GetTargetsUseCase.Params())
            .collect {
                when (it) {
                    is Resource.Success -> {
                        _isLoading.postValue(false)
                        _targets.postValue(it.data)
                    }
                    is Resource.Error -> {
                        _error.postValue(it.error)
                        _isLoading.postValue(false)
                    }
                    is Resource.Loading -> {
                        _isLoading.postValue(true)
                    }
                }
            }
    }

    fun isDoneEnabled(): Boolean {
        return _isDoneEnabled.value ?: false
    }

    fun getSelectedTargets(): Array<Target> {
        return _selectedTargets.value?.toTypedArray() ?: arrayOf()
    }

    fun toggleTarget(target: Target) = viewModelScope.run {
        val selectedTargets = _selectedTargets.value?.toMutableSet() ?: mutableSetOf()
        if (selectedTargets.contains(target)) {
            selectedTargets.remove(target)
        } else {
            selectedTargets.add(target)
        }
        _isDoneEnabled.value = selectedTargets.isNotEmpty()
        _selectedTargets.value = selectedTargets
    }

}