package com.test.marketingcampaign.ui.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.presentation.base.BaseFragment
import com.test.marketingcampaign.core.presentation.extensions.drawable
import com.test.marketingcampaign.core.presentation.extensions.sendEmail
import com.test.marketingcampaign.databinding.FragmentReviewBinding
import javax.inject.Inject

class ReviewFragment @Inject constructor() : BaseFragment() {

    private val args: ReviewFragmentArgs by navArgs()
    private val viewModel: ReviewViewModel by viewModels { viewModelFactory }
    private lateinit var reviewAdapter: ReviewAdapter

    private var _binding: FragmentReviewBinding? = null
    private val binding get() = _binding ?: throw Exception("Expected binding not null")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setCampaigns(args.reviewCampaigns)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentReviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setToolbarTitle(getString(R.string.title_review_purchase))
        setupReviewRecyclerView()
        setupControls()
        observe()
    }

    private fun setupReviewRecyclerView() = binding.run {
        reviewAdapter = ReviewAdapter()
        rvReviewCampaigns.apply {
            adapter = reviewAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL).apply {
                setDrawable(context.drawable(R.drawable.divider_space_10))
            })
        }
    }

    private fun setupControls() = binding.run {
        btConfirm.setOnClickListener {
            requireActivity().sendEmail(
                getString(R.string.bogus_email),
                "Marketing campaigns purchase",
                viewModel.getReviewEmailContent()
            )
        }
    }

    private fun observe() = viewModel.run {
        campaigns.observe(viewLifecycleOwner, Observer {
            reviewAdapter.submitList(it)
        })
        total.observe(viewLifecycleOwner, Observer {
            binding.tvTotal.text = getString(R.string.label_total, "$it")
        })
    }
}