package com.test.marketingcampaign.ui.review

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.test.marketingcampaign.core.domain.entities.CampaignReview
import com.test.marketingcampaign.core.presentation.base.BaseViewModel
import javax.inject.Inject

class ReviewViewModel @Inject constructor() : BaseViewModel() {

    val campaigns: LiveData<List<CampaignReview>> get() = _campaigns
    private val _campaigns = MutableLiveData<List<CampaignReview>>()

    val total: LiveData<Double> get() = _total
    private val _total = MediatorLiveData<Double>()

    init {
        _total.addSource(campaigns) {
            var total = 0.0
            it.forEach { total += it.campaign.valueInEuro }
            _total.value = total
        }
    }

    fun setCampaigns(campaigns: Array<CampaignReview>) {
        _campaigns.value = campaigns.toList()
    }

    fun getReviewEmailContent(): String {
        var result = ""
        campaigns.value?.forEach {
            result += "${it.channelName}:\nTargets:"
            it.availableTargets.forEachIndexed { index, target ->
                if (index > 0) result += ", "
                result += target.name
            }
            result += "\nCampaign: ${it.campaign.slug} Price: ${it.campaign.valueInEuro} EUR\n"
        }
        result += "\nTotal: ${_total.value ?: 0.0}"
        return result
    }

}