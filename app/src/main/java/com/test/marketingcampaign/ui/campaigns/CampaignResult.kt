package com.test.marketingcampaign.ui.campaigns

import android.os.Parcelable
import com.test.marketingcampaign.core.domain.entities.Campaign
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CampaignResult(
    val channelSlug: String,
    val campaign: Campaign?
) : Parcelable {
    companion object {
        const val TAG = "CampaignResult"
    }
}