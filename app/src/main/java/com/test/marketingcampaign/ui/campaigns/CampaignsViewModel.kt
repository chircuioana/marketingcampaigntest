package com.test.marketingcampaign.ui.campaigns

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.domain.interactors.GetCampaignsForChannelUseCase
import com.test.marketingcampaign.core.domain.interactors.GetChannelsForTargetsUseCase
import com.test.marketingcampaign.core.presentation.base.BaseViewModel
import com.test.marketingcampaign.core.utils.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class CampaignsViewModel @Inject constructor(
    private val getCampaignsForChannelUseCase: GetCampaignsForChannelUseCase
) : BaseViewModel() {

    val campaigns: LiveData<List<Campaign>> get() = _campaigns
    private val _campaigns = MutableLiveData<List<Campaign>>()

    val selectedCampaign: LiveData<Campaign?> get() = _selectedCampaign
    private val _selectedCampaign = MutableLiveData<Campaign?>()

    private var channel: String = ""

    fun fetchChannelCampaigns(channel: String, selectedCampaignSlug: String?) =
        viewModelScope.launch {
            this@CampaignsViewModel.channel = channel

            getCampaignsForChannelUseCase.execute(GetCampaignsForChannelUseCase.Params(channel))
                .collect {
                    when (it) {
                        is Resource.Success -> {
                            _isLoading.postValue(false)
                            it.data.let { campaigns ->
                                campaigns.find { it.slug == selectedCampaignSlug }?.let {
                                    _selectedCampaign.postValue(it)
                                } ?: run { _selectedCampaign.postValue(null) }
                                _campaigns.postValue(campaigns)
                            }
                        }
                        is Resource.Error -> {
                            _error.postValue(it.error)
                            _isLoading.postValue(false)
                        }
                        is Resource.Loading -> {
                            _isLoading.postValue(true)
                        }
                    }
                }
        }

    fun setSelectedCampaign(campaign: Campaign?) {
        _selectedCampaign.value = campaign
    }

    fun getSelectedCampaignResult(): CampaignResult? =
        CampaignResult(channel, _selectedCampaign.value)
}