package com.test.marketingcampaign.ui.channels

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.*
import androidx.core.text.color
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.presentation.base.BaseFragment
import com.test.marketingcampaign.core.presentation.extensions.color
import com.test.marketingcampaign.core.presentation.extensions.drawable
import com.test.marketingcampaign.core.presentation.extensions.toast
import com.test.marketingcampaign.databinding.FragmentChannelsBinding
import com.test.marketingcampaign.ui.campaigns.CampaignResult
import com.test.marketingcampaign.utils.getNavigationResult
import javax.inject.Inject

class ChannelsFragment @Inject constructor() : BaseFragment() {

    private val args: ChannelsFragmentArgs by navArgs()
    private val viewModel: ChannelsViewModel by viewModels { viewModelFactory }
    private lateinit var channelsAdapter: ChannelsAdapter

    private var _binding: FragmentChannelsBinding? = null
    private val binding get() = _binding ?: throw Exception("Expected binding not null")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        viewModel.fetchChannelsForTargets(args.targets)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_channels, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.review_action -> {
                findNavController().navigate(
                    ChannelsFragmentDirections.actionChannelsFragmentToReviewFragment(viewModel.getReviewCampaigns())
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val color = if (viewModel.isReviewEnabled()) {
            requireContext().color(R.color.blue_3F51B5)
        } else {
            requireContext().color(R.color.grey_CCCCCC)
        }
        menu.getItem(0).apply {
            isEnabled = viewModel.isReviewEnabled()
            title =
                SpannableStringBuilder()
                    .color(color)
                    { append(getString(R.string.label_review)) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChannelsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        getNavigationResult<CampaignResult>(R.id.channelsFragment, CampaignResult.TAG) {
            viewModel.updateChannelForms(it.channelSlug, it.campaign)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setToolbarTitle(getString(R.string.title_channels))
        setupChannelsRecyclerView()
        observe()
    }

    private fun setupChannelsRecyclerView() = binding.run {
        channelsAdapter = ChannelsAdapter(onChannelSelected = {
            findNavController().navigate(
                ChannelsFragmentDirections.actionChannelsFragmentToCampaignsFragment(
                    it.channelSlug, it.channelName, it.campaign?.slug ?: ""
                )
            )
        })
        rvChannels.apply {
            adapter = channelsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL).apply {
                setDrawable(requireContext().drawable(R.drawable.divider_space_10))
            })
        }
    }

    private fun observe() = viewModel.run {
        isLoading.observe(viewLifecycleOwner, Observer {
            showLoading(it)
        })
        error.observe(viewLifecycleOwner, Observer {
            //TODO
            toast("Warning! An error occurred!")
        })
        channels.observe(viewLifecycleOwner, Observer {
            channelsAdapter.submitList(it)
        })
        isReviewEnabled.observe(viewLifecycleOwner, Observer {
            requireActivity().invalidateOptionsMenu()
        })
    }
}