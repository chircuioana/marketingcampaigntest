package com.test.marketingcampaign.ui.campaigns

import android.os.Bundle
import android.os.Handler
import android.text.SpannableStringBuilder
import android.view.*
import androidx.core.text.color
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.presentation.base.BaseFragment
import com.test.marketingcampaign.core.presentation.extensions.color
import com.test.marketingcampaign.core.presentation.extensions.toast
import com.test.marketingcampaign.databinding.FragmentCampaignsBinding
import com.test.marketingcampaign.utils.setNavigationResult
import java.util.*
import javax.inject.Inject

class CampaignsFragment @Inject constructor() : BaseFragment() {

    private val args: CampaignsFragmentArgs by navArgs()
    private val viewModel: CampaignsViewModel by viewModels { viewModelFactory }
    private lateinit var campaignsAdapter: CampaignsAdapter

    private var _binding: FragmentCampaignsBinding? = null
    private val binding get() = _binding ?: throw Exception("Expected binding not null")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.fetchChannelCampaigns(args.channelSlug, args.selectedCampaignSlug)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_campaign, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.done_action -> {
                setNavigationResult(CampaignResult.TAG, viewModel.getSelectedCampaignResult())
                findNavController().popBackStack()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val color = requireContext().color(R.color.blue_3F51B5)
        menu.getItem(0).apply {
            title =
                SpannableStringBuilder()
                    .color(color)
                    { append(getString(R.string.label_done)) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCampaignsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setToolbarTitle(getString(R.string.title_campaigns,args.channelName).capitalize(Locale.getDefault()))

        setupCampaignRecyclerView()
        observe()
    }

    private fun setupCampaignRecyclerView() = binding.run {
        campaignsAdapter = CampaignsAdapter(onCampaignSelected = {
            viewModel.setSelectedCampaign(it)
        })
        rvCampaigns.apply {
            adapter = campaignsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            //GridLayoutManager(requireContext(), 2)
        }
    }

    private fun observe() = viewModel.run {
        isLoading.observe(viewLifecycleOwner, Observer {
            showLoading(it)
        })
        error.observe(viewLifecycleOwner, Observer {
            //TODO
            toast("Warning! An error occurred!")
        })
        campaigns.observe(viewLifecycleOwner, Observer {
            campaignsAdapter.submitList(it)
            Handler().post {
                binding.rvCampaigns.scrollToPosition(0)
            }
        })
        selectedCampaign.observe(viewLifecycleOwner, Observer {
            campaignsAdapter.selectedCampaign = it
        })
    }
}