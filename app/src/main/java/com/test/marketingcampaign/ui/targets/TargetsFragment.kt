package com.test.marketingcampaign.ui.targets

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.*
import androidx.core.text.color
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.presentation.base.BaseFragment
import com.test.marketingcampaign.core.presentation.extensions.color
import com.test.marketingcampaign.core.presentation.extensions.drawable
import com.test.marketingcampaign.core.presentation.extensions.toast
import com.test.marketingcampaign.databinding.FragmentTargetsBinding
import com.xiaofeng.flowlayoutmanager.Alignment
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import javax.inject.Inject

class TargetsFragment @Inject constructor() : BaseFragment() {
    private val viewModel: TargetsViewModel by viewModels { viewModelFactory }
    private lateinit var targetsAdapter: TargetsAdapter

    private var _binding: FragmentTargetsBinding? = null
    private val binding get() = _binding ?: throw Exception("Expected binding not null")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        viewModel.fetchTargets()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_targets, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.next_action -> {
                findNavController().navigate(
                    TargetsFragmentDirections.actionTargetsFragmentToChannelsFragment(viewModel.getSelectedTargets())
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val color = if (viewModel.isDoneEnabled()) {
            requireContext().color(R.color.blue_3F51B5)
        } else {
            requireContext().color(R.color.grey_CCCCCC)
        }
        menu.getItem(0).apply {
            isEnabled = viewModel.isDoneEnabled()
            title =
                SpannableStringBuilder()
                    .color(color)
                    { append(getString(R.string.label_next)) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTargetsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setToolbarTitle(getString(R.string.title_targets))
        setupTargetsRecyclerView()
        observe()
    }

    private fun setupTargetsRecyclerView() = binding.run {
        targetsAdapter = TargetsAdapter(onToggleTarget = {
            viewModel.toggleTarget(it)
        })
        rvTargets.apply {
            adapter = targetsAdapter
            layoutManager = FlowLayoutManager().apply {
                removeItemPerLineLimit()
                setAlignment(Alignment.LEFT)
            }
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    RecyclerView.HORIZONTAL
                ).apply {
                    setDrawable(context.drawable(R.drawable.divider_space_10))
                })
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    RecyclerView.VERTICAL
                ).apply {
                    setDrawable(context.drawable(R.drawable.divider_space_10))
                })
        }
    }

    private fun observe() = viewModel.run {
        isLoading.observe(viewLifecycleOwner, Observer {
            showLoading(it)
        })
        error.observe(viewLifecycleOwner, Observer {
            //TODO
            toast("Warning! An error occurred!")
        })
        targets.observe(viewLifecycleOwner, Observer {
            targetsAdapter.submitList(it)
        })
        selectedTargets.observe(viewLifecycleOwner, Observer {
            targetsAdapter.selectedTargets = it
        })
        isDoneEnabled.observe(viewLifecycleOwner, Observer {
            requireActivity().invalidateOptionsMenu()
        })
    }
}