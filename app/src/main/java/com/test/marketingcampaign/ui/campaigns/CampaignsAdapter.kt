package com.test.marketingcampaign.ui.campaigns

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.test.marketingcampaign.R
import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.presentation.extensions.drawable
import com.test.marketingcampaign.databinding.ItemCampaignBinding
import com.test.marketingcampaign.databinding.ItemNoCampaignBinding

private const val NO_CAMPAIGN_TYPE = 0
private const val CAMPAIGN_TYPE = 1

class CampaignsAdapter(
    private val onCampaignSelected: (Campaign?) -> Unit
) : ListAdapter<Campaign, CampaignsAdapter.AbstractViewHolder>(Campaign.CampaignDiffCallback()) {

    var selectedCampaign: Campaign? = null
        set(value) {
            if (field == value) return
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder =
        when (viewType) {
            NO_CAMPAIGN_TYPE -> NoCampaignViewHolder(
                ItemNoCampaignBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            CAMPAIGN_TYPE
            -> CampaignViewHolder(
                ItemCampaignBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> throw Exception("Unknown view type")
        }


    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        when (holder) {
            is NoCampaignViewHolder -> {
                holder.bind()
            }
            is CampaignViewHolder -> {
                holder.bind(getItem(position - 1))
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (position == 0) NO_CAMPAIGN_TYPE else CAMPAIGN_TYPE

    override fun getItemCount(): Int =
        if (super.getItemCount() == 0) 0 else super.getItemCount() + 1

    abstract inner class AbstractViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class NoCampaignViewHolder(private val itemBinding: ItemNoCampaignBinding) :
        AbstractViewHolder(itemBinding.root) {
        fun bind() = itemBinding.run {
            if (selectedCampaign == null) {
                btSelect.isEnabled = false
                btSelect.setText(R.string.label_selected)
                btSelect.setOnClickListener(null)
                return@run
            }
            btSelect.isEnabled = true
            btSelect.setText(R.string.label_select)
            btSelect.setOnClickListener {
                onCampaignSelected(null)
            }
        }
    }

    inner class CampaignViewHolder(private val itemBinding: ItemCampaignBinding) :
        AbstractViewHolder(itemBinding.root) {
        fun bind(campaign: Campaign) = itemBinding.run {
            tvCampaignName.text = campaign.displayName
            tvCampaignName.background = itemView.context.drawable(campaign.getBackground())
            tvCampaignDescription.text = campaign.description

            if (selectedCampaign == campaign) {
                btSelect.isEnabled = false
                btSelect.setText(R.string.label_selected)
                btSelect.setOnClickListener(null)
                return@run
            }
            btSelect.isEnabled = true
            btSelect.setText(R.string.label_select)
            btSelect.setOnClickListener {
                onCampaignSelected(campaign)
            }
        }
    }
}