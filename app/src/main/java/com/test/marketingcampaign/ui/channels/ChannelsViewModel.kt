package com.test.marketingcampaign.ui.channels

import androidx.lifecycle.*
import com.test.marketingcampaign.core.domain.entities.Campaign
import com.test.marketingcampaign.core.domain.entities.CampaignReview
import com.test.marketingcampaign.core.domain.entities.ChannelForm
import com.test.marketingcampaign.core.domain.entities.Target
import com.test.marketingcampaign.core.domain.interactors.GetChannelsForTargetsUseCase
import com.test.marketingcampaign.core.presentation.base.BaseViewModel
import com.test.marketingcampaign.core.utils.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class ChannelsViewModel @Inject constructor(
    private val getChannelsForTargetsUseCase: GetChannelsForTargetsUseCase
) : BaseViewModel() {

    val channels: LiveData<List<ChannelForm>> get() = _channels
    private val _channels = MutableLiveData<List<ChannelForm>>()

    val isReviewEnabled: LiveData<Boolean>
        get() = Transformations.distinctUntilChanged(
            _isReviewEnabled
        )
    private val _isReviewEnabled = MediatorLiveData<Boolean>()

    private var targets: Array<Target> = emptyArray()

    init {
        _isReviewEnabled.value = false
        _isReviewEnabled.addSource(channels) { channelForms ->
            _isReviewEnabled.value = channelForms.find { it.campaign != null } != null
        }
    }

    fun fetchChannelsForTargets(targets: Array<Target>) = viewModelScope.launch {
        this@ChannelsViewModel.targets = targets

        getChannelsForTargetsUseCase.execute(GetChannelsForTargetsUseCase.Params(targets))
            .collect {
                when (it) {
                    is Resource.Success -> {
                        _isLoading.postValue(false)
                        _channels.postValue(it.data)
                    }
                    is Resource.Error -> {
                        _error.postValue(it.error)
                        _isLoading.postValue(false)
                    }
                    is Resource.Loading -> {
                        _isLoading.postValue(true)
                    }
                }
            }
    }

    fun updateChannelForms(channelSlug: String, campaign: Campaign?) {
        val forms = _channels.value ?: return
        forms.find { it.channelSlug == channelSlug }?.apply { this.campaign = campaign }
        _channels.value = forms
    }

    fun isReviewEnabled(): Boolean {
        return _isReviewEnabled.value ?: false
    }

    fun getReviewCampaigns(): Array<CampaignReview> {
        return _channels.value
            ?.filter { it.campaign != null }
            ?.map {
                CampaignReview(
                    it.channelSlug,
                    it.channelName,
                    it.availableTargets,
                    it.campaign!!
                )
            }?.toTypedArray()
            ?: emptyArray()
    }
}