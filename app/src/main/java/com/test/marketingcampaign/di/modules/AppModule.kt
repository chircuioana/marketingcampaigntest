package com.test.marketingcampaign.di.modules

import android.content.Context
import com.test.marketingcampaign.ui.App
import dagger.Module
import dagger.Provides

@Module
open class AppModule {

    @Provides
    fun provideContext(application: App): Context {
        return application.applicationContext
    }
}