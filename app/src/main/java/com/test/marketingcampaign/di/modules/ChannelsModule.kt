package com.test.marketingcampaign.di.modules

import androidx.lifecycle.ViewModel
import com.test.marketingcampaign.di.scopes.ChildFragmentScoped
import com.test.marketingcampaign.di.scopes.ViewModelKey
import com.test.marketingcampaign.ui.channels.ChannelsFragment
import com.test.marketingcampaign.ui.channels.ChannelsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class ChannelsModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun channelsFragment(): ChannelsFragment

    @Binds
    @IntoMap
    @ViewModelKey(ChannelsViewModel::class)
    abstract fun bindChannelsViewModel(viewModel: ChannelsViewModel): ViewModel
}