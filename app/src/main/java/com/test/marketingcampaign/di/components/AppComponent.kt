package com.test.marketingcampaign.di.components

import com.test.marketingcampaign.core.data.di.ApiModule
import com.test.marketingcampaign.core.data.di.DataModule
import com.test.marketingcampaign.di.modules.ActivityBindingModule
import com.test.marketingcampaign.di.modules.AppModule
import com.test.marketingcampaign.di.modules.CoroutinesModule
import com.test.marketingcampaign.di.modules.ViewModelModule
import com.test.marketingcampaign.ui.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        AppModule::class,
        CoroutinesModule::class,
        DataModule::class,
        ApiModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: App): AppComponent
    }
}