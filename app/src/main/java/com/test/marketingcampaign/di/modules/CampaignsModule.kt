package com.test.marketingcampaign.di.modules

import androidx.lifecycle.ViewModel
import com.test.marketingcampaign.di.scopes.ChildFragmentScoped
import com.test.marketingcampaign.di.scopes.ViewModelKey
import com.test.marketingcampaign.ui.campaigns.CampaignsFragment
import com.test.marketingcampaign.ui.campaigns.CampaignsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class CampaignsModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun campaignsFragment(): CampaignsFragment

    @Binds
    @IntoMap
    @ViewModelKey(CampaignsViewModel::class)
    abstract fun bindCampaignsViewModel(viewModel: CampaignsViewModel): ViewModel
}