package com.test.marketingcampaign.di.modules

import androidx.lifecycle.ViewModelProvider
import com.test.marketingcampaign.di.AppViewModelFactory
import dagger.Binds
import dagger.Module

@Module
@Suppress("UNUSED")
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: AppViewModelFactory):
            ViewModelProvider.Factory
}