package com.test.marketingcampaign.di.modules

import androidx.lifecycle.ViewModel
import com.test.marketingcampaign.di.scopes.ChildFragmentScoped
import com.test.marketingcampaign.di.scopes.ViewModelKey
import com.test.marketingcampaign.ui.review.ReviewFragment
import com.test.marketingcampaign.ui.review.ReviewViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class ReviewModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun reviewFragment(): ReviewFragment

    @Binds
    @IntoMap
    @ViewModelKey(ReviewViewModel::class)
    abstract fun bindReviewViewModel(viewModel: ReviewViewModel): ViewModel
}