package com.test.marketingcampaign.di.modules

import com.test.marketingcampaign.di.scopes.ActivityScoped
import com.test.marketingcampaign.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            // activity
            MainActivityModule::class,
            // fragments
            TargetsModule::class,
            ChannelsModule::class,
            CampaignsModule::class,
            ReviewModule::class
        ]
    )
    internal abstract fun mainActivity(): MainActivity
}