package com.test.marketingcampaign.di.scopes

import javax.inject.Scope

@MustBeDocumented
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ChildFragmentScoped