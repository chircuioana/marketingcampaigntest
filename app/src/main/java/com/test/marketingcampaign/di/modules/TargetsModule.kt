package com.test.marketingcampaign.di.modules

import androidx.lifecycle.ViewModel
import com.test.marketingcampaign.di.scopes.ChildFragmentScoped
import com.test.marketingcampaign.di.scopes.ViewModelKey
import com.test.marketingcampaign.ui.targets.TargetsFragment
import com.test.marketingcampaign.ui.targets.TargetsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class TargetsModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun targetsFragment(): TargetsFragment

    @Binds
    @IntoMap
    @ViewModelKey(TargetsViewModel::class)
    abstract fun bindTargetsViewModel(viewModel: TargetsViewModel): ViewModel
}